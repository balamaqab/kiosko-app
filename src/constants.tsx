export const APP_VERSION = process.env.REACT_APP_VERSION;
export const APP_NAME = process.env.REACT_APP_NAME;
export const CMS_URL = process.env.REACT_APP_CMS_URL;
export const CMS_AK = process.env.REACT_APP_CMS_AK;
export const CMS_API = `${CMS_URL}/api`;
export const CMS_UPLOADS = `${CMS_URL}/uploads`;
export const ROUTES = {
  category: '/tema/',
  article: '/articulo/',
  keyword: '/etiqueta/',
  search: '/busqueda/',
};
