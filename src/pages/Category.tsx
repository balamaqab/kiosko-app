import { IonCol, IonContent, IonFooter, IonHeader, IonPage, IonRow } from '@ionic/react';
import { RouteComponentProps } from 'react-router';
import ArticleList from '../components/ArticleList';
import KskHeader from '../components/Header';
import Searchbar from '../components/Searchbar';
import StrapiQuery from '../components/StrapiQuery';

import './Page.css';

interface CategoryPageProps
  extends RouteComponentProps<{
    title?: string;
    slug: string;
  }> {}

const CategoryPage: React.FC<CategoryPageProps> = (props) => {
  const slug = props.match.params.slug;
  return (
    <IonPage>
      <IonHeader>
        <KskHeader {...props} />
      </IonHeader>
      <IonContent fullscreen>
        <IonRow className="ion-margin">
          <IonCol>{props.match.params.title && <h2>{props.match.params.title}</h2>}</IonCol>
        </IonRow>

        <StrapiQuery
          endpoint="articles"
          customQuery={{
            fields: ['slug', 'title', 'id'],
            populate: {
              featured_image: {
                fields: ['url'],
              },
              category: {
                fields: ['slug'],
              },
            },
            filters: {
              category: {
                slug: {
                  $eq: slug,
                },
              },
            },
          }}
        >
          {(content: any) => <ArticleList data={content} />}
        </StrapiQuery>
      </IonContent>
      <IonFooter>
        <Searchbar />
      </IonFooter>
    </IonPage>
  );
};

export default CategoryPage;
