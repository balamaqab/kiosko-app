import { IonCol, IonContent, IonFooter, IonHeader, IonPage, IonRow } from '@ionic/react';
import StrapiQuery from '../components/StrapiQuery';
import KskHeader from '../components/Header';
import { RouteComponentProps } from 'react-router';
import ArticleList from '../components/ArticleList';

import './Page.css';
import Searchbar from '../components/Searchbar';
import KeywordList from '../components/KeywordList';
import { SchemaKeyword } from '../schemas/keyword';
import { SchemaArticle } from '../schemas/article';
import { SchemaCategory } from '../schemas/category';
import CategoryList from '../components/CategoryList';

interface SearchResultPageProps
  extends RouteComponentProps<{
    term: string;
  }> {}

const SearchResultPage: React.FC<SearchResultPageProps> = (props) => {
  const term = props.match.params.term;
  return (
    <IonPage>
      <IonHeader>
        <KskHeader {...props} />
      </IonHeader>
      <IonContent fullscreen>
        <IonRow className="ion-margin">
          <IonCol>
            <span className="ion-no-margin">Resultados de búsqueda:</span>
            <h2 className="ion-no-margin ion-text-capitalize">{term}</h2>
            <hr />
          </IonCol>
        </IonRow>
        <StrapiQuery
          endpoint="articles"
          customQuery={{
            fields: ['slug', 'title', 'id'],
            populate: {
              featured_image: {
                fields: ['url'],
              },
              category: {
                fields: ['slug'],
              },
            },
            filters: {
              $or: [
                {
                  title: {
                    $containsi: term,
                  },
                },
                {
                  content: {
                    $containsi: term,
                  },
                },
              ],
            },
          }}
        >
          {(content: Array<SchemaArticle>) =>
            content &&
            content?.length > 0 && (
              <IonRow className="ion-margin">
                <IonCol>
                  <h4 className="ion-no-margin">Artículos clave</h4>
                  <ArticleList data={content} />
                </IonCol>
              </IonRow>
            )
          }
        </StrapiQuery>

        <hr />

        <StrapiQuery
          endpoint="categories"
          customQuery={{
            populate: '*',
            filters: {
              name: {
                $containsi: term,
              },
            },
          }}
        >
          {(content: Array<SchemaCategory>) =>
            content &&
            content?.length > 0 && (
              <IonRow className="ion-margin">
                <IonCol>
                  <h4 className="ion-no-margin">Categorías</h4>
                  <CategoryList data={content} />
                </IonCol>
              </IonRow>
            )
          }
        </StrapiQuery>
        <hr />

        <StrapiQuery
          endpoint="keywords"
          customQuery={{
            populate: '*',
            filters: {
              keyword: {
                $containsi: term,
              },
            },
          }}
        >
          {(content: Array<SchemaKeyword>) =>
            content &&
            content?.length > 0 && (
              <IonRow className="ion-margin">
                <IonCol>
                  <h4 className="ion-no-margin">Palabras clave</h4>
                  <KeywordList data={content} />
                </IonCol>
              </IonRow>
            )
          }
        </StrapiQuery>
      </IonContent>
      <IonFooter>
        <Searchbar term={term} />
      </IonFooter>
    </IonPage>
  );
};

export default SearchResultPage;
