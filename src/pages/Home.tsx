import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import StrapiQuery from '../components/StrapiQuery';
import KskHeader from '../components/Header';
import { RouteComponentProps } from 'react-router';
import HomeList from '../components/HomeList';
import Searchbar from '../components/Searchbar';

import './Page.css';
import LastLine from '../components/LastLine';

const HomePage: React.FC<RouteComponentProps> = (props) => {
  return (
    <IonPage>
      <IonHeader>
        <KskHeader {...props} />
      </IonHeader>
      <IonContent className="pageHome" fullscreen>
        <StrapiQuery
          endpoint="homepage-item"
          customQuery={{
            populate: {
              item: {
                populate: {
                  icon: '*',
                  description: '*',
                  article: { fields: ['slug', 'title'] },
                  keyword: { fields: ['slug', 'keyword'] },
                  category: { fields: ['slug', 'name'] },
                },
              },
            },
          }}
        >
          {(content: any) => <HomeList data={content} />}
        </StrapiQuery>

        <LastLine />
      </IonContent>
      <IonFooter>
        <Searchbar />
      </IonFooter>
    </IonPage>
  );
};

export default HomePage;
