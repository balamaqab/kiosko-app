import { IonCol, IonContent, IonFooter, IonHeader, IonPage, IonRow } from '@ionic/react';
import { RouteComponentProps } from 'react-router';
import ArticleList from '../components/ArticleList';
import KskHeader from '../components/Header';
import Searchbar from '../components/Searchbar';
import StrapiQuery from '../components/StrapiQuery';

import './Page.css';

interface KeywordPageProps
  extends RouteComponentProps<{
    title?: string;
    slug: string;
  }> {}

const KeywordPage: React.FC<KeywordPageProps> = (props) => {
  const slug = props.match.params.slug;
  return (
    <IonPage>
      <IonHeader>
        <KskHeader {...props} />
      </IonHeader>
      <IonContent fullscreen>
        <IonRow className="ion-margin">
          <IonCol>{props.match.params.title && <h2 className="ksk-capitalize">#{props.match.params.title}</h2>}</IonCol>
        </IonRow>

        <StrapiQuery
          endpoint="articles"
          customQuery={{
            fields: ['slug', 'title', 'id'],
            populate: {
              featured_image: {
                fields: ['url'],
              },
              category: {
                fields: ['slug'],
              },
            },
            filters: {
              keywords: {
                slug: {
                  $eq: slug,
                },
              },
            },
          }}
        >
          {(content: any) => <ArticleList data={content} />}
        </StrapiQuery>
      </IonContent>
      <IonFooter>
        <Searchbar />
      </IonFooter>
    </IonPage>
  );
};

export default KeywordPage;
