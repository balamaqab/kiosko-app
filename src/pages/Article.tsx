import { IonContent, IonHeader, IonPage } from '@ionic/react';
import { RouteComponentProps } from 'react-router';
import KskHeader from '../components/Header';
import StrapiQuery from '../components/StrapiQuery';
import ArticleContent from '../components/ArticleContent';

import './Page.css';

interface ArticlePageProps
  extends RouteComponentProps<{
    slug: string;
  }> {}

const ArticlePage: React.FC<ArticlePageProps> = (props) => {
  const slug = props.match.params.slug;
  return (
    <IonPage>
      <IonHeader>
        <KskHeader {...props} />
      </IonHeader>
      <IonContent fullscreen>
        <StrapiQuery
          endpoint="articles"
          customQuery={{
            fields: '*',
            populate: '*',
            filters: {
              slug: {
                $eq: slug,
              },
            },
            pagination: { limit: 1 },
          }}
        >
          {(content: any) => <ArticleContent data={content} />}
        </StrapiQuery>
      </IonContent>
    </IonPage>
  );
};

export default ArticlePage;
