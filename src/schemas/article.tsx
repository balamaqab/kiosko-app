import { SchemaCategory } from './category';
import { SchemaKeyword } from './keyword';
import { SchemaMedia } from './media';

export type SchemaArticle = {
  id: number;
  title: string;
  slug: string;
  content?: string;
  featured_image?: SchemaMedia;
  locale?: string;
  createdAt?: string;
  publishedAt?: string;
  updatedAt?: string;
  category?: SchemaCategory;
  keywords?: Array<SchemaKeyword>;
};
