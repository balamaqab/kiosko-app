export type SchemaMedia = {
  id: number;
  name?: string;
  url?: string;
  alternativeText?: string;
  caption?: string;
  createdAt?: string;
  ext?: string;
  formats?: any;
  hash?: string;
  height?: number;
  mime?: string;
  previewUrl?: string | null;
  provider?: string;
  provider_metadata?: string | null;
  size?: number;
  updatedAt?: string;
  width?: number;
};
