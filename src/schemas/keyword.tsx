export type SchemaKeyword = {
  id: number;
  slug?: string;
  keyword?: string;
  locale?: string;
  createdAt?: string;
  updatedAt?: string;
};
