import { SchemaMedia } from './media';

export type SchemaCategory = {
  id: string;
  slug?: string;
  icon?: SchemaMedia;
  name?: string;
  description?: string;
  locale?: string;
  order?: number;
  createdAt?: string;
  updatedAt?: string;
};
