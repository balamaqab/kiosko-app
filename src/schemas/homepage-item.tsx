import { SchemaArticle } from './article';
import { SchemaCategory } from './category';
import { SchemaKeyword } from './keyword';
import { SchemaMedia } from './media';

export type SchemaHomepage = {
  id: string;
  item?: Array<SchemaHomepageItem>;
  createdAt?: string;
  publishedAt?: string;
  updatedAt?: string;
};

export type SchemaHomepageItem = {
  id: string;
  icon?: SchemaMedia;
  description?: string;
  category?: SchemaCategory;
  keyword?: SchemaKeyword;
  article?: SchemaArticle;
};
