import qs from 'qs';

export type SharpModifiers = {
  format?: 'jpg' | 'jpeg' | 'png' | 'webp' | 'avif' | 'gif' | 'heif';
  width?: number;
  w?: number;
  height?: number;
  h?: number;
  resize?: string;
  fit?: 'cover' | 'contain';
  position?: 'top' | 'right top' | 'right' | 'right bottom' | 'bottom' | 'left bottom' | 'left' | 'left top';
  rotate?: number;
  strategy?: 'attention' | 'entropy';
  grayscale?: boolean;
  flip?: boolean;
  flop?: boolean;
};

export const getImageSizeUrl = (url: string, modifiers: SharpModifiers): string => {
  const query = qs.stringify({ fit: 'cover', format: 'jpg', ...modifiers });
  return `${url}?${query}`;
};

export const prefetchImage = (url: string) => {
  const image = new Image();
  image.src = url;
};

export const prefetchImages = (images: Array<any>) => {
  images.forEach((image) => {
    if (image.url) {
      prefetchImage(image.url);
    }
  });
};
