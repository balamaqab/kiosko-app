import { IonIcon, IonItem, IonLabel, IonList } from '@ionic/react';
import { chevronForwardCircleOutline } from 'ionicons/icons';

const generateKey = (pre: string = 'li') => {
  return `${pre}_${new Date().getTime()}_${Math.round(Math.random() * 1000)}`;
};

export const renderer: any = {
  list(body: string, ordered: boolean, start: number) {
    return (
      <IonList key={generateKey()} className="simple">
        {body}
      </IonList>
    );
  },
  listItem(text: string, task: boolean, checked: boolean) {
    return (
      <IonItem key={generateKey()} className="simple">
        <IonIcon className="ion-margin-end" icon={chevronForwardCircleOutline} size="small" color="secondary" />
        <IonLabel>{text}</IonLabel>
      </IonItem>
    );
  },
};
