import { CMS_AK, CMS_API } from "../constants";

export const fetcherWithToken = (endpoint: string) =>
  fetch(`${CMS_API}${endpoint}`, {
    headers: {
      Authorization: `Bearer ${CMS_AK}`,
    },
  }).then((res) => res.json());

export const fetcher = (endpoint: string) =>
  fetch(`${CMS_API}${endpoint}`).then((res) => res.json());
