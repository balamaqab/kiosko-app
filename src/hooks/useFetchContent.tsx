import useSWRImmutable from 'swr/immutable';
import qs from 'qs';

type FetchContentProps = {
  endpoint: string;
  query?: any;
};

const useFetchContent = ({ endpoint, query }: FetchContentProps) => {
  const q = qs.stringify(query, {
    encodeValuesOnly: true,
  });
  const e = `${endpoint}?${q}`;
  const { data, error } = useSWRImmutable(e);
  return {
    content: data ? data.data : data,
    isLoading: !error && !data,
    isError: error,
  };
};

export default useFetchContent;
