import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import HomePage from './pages/Home';
import CategoryPage from './pages/Category';
import ArticlePage from './pages/Article';
import KeywordPage from './pages/Keyword';
import { fetcher } from './helpers/fetchers';
import { SWRConfig } from 'swr';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/fonts.css';
import './theme/variables.css';
import SearchResultPage from './pages/SearchResults';
import { ROUTES } from './constants';

setupIonicReact();

const App: React.FC = () => (
  <SWRConfig
    value={{
      fetcher: fetcher,
    }}
  >
    <IonApp>
      <IonReactRouter>
        <IonRouterOutlet>
          <Route exact path="/inicio" component={HomePage} />
          <Route path={ROUTES.category + ':slug/:title?'} component={CategoryPage} />
          <Route path={ROUTES.keyword + ':slug/:title?'} component={KeywordPage} />
          <Route path={ROUTES.article + ':slug'} component={ArticlePage} />
          <Route path={ROUTES.search + ':term'} component={SearchResultPage} />
          <Route exact path="/">
            <Redirect to="/inicio" />
          </Route>
        </IonRouterOutlet>
      </IonReactRouter>
    </IonApp>
  </SWRConfig>
);

export default App;
