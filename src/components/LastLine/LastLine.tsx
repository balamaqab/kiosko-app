import { IonImg, IonRouterLink } from '@ionic/react';
import { APP_VERSION, ROUTES } from '../../constants';

import './LastLine.css';

const LastLine: React.FC = () => {
  return (
    <div className="lastLine">
      {' '}
      <div className="words"></div>
      <IonImg className="logo" src="/assets/yooguatemala-mono.png" alt="Yo'o Guatemala" />
      <div className="words">
        Kioskito v{APP_VERSION} &mdash;&nbsp;{' '}
        <IonRouterLink routerDirection="forward" routerLink={`${ROUTES.article}acerca-de`}>
          Hecho con 💚
        </IonRouterLink>
      </div>
    </div>
  );
};

export default LastLine;
