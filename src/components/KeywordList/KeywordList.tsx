import { IonChip, IonRouterLink } from '@ionic/react';
import { SchemaKeyword } from '../../schemas/keyword';

type KeywordListProps = {
  data?: Array<SchemaKeyword>;
};

const KeywordList: React.FC<KeywordListProps> = ({ data }) => {
  return (
    <>
      {data &&
        data.map(({ slug, keyword }: SchemaKeyword) => {
          const title = keyword ? `/${encodeURI(keyword)}` : '';
          return (
            <IonRouterLink key={`keyword-${slug}`} routerDirection="forward" routerLink={`/etiqueta/${slug}${title}`}>
              <IonChip color="secondary">#{keyword}</IonChip>
            </IonRouterLink>
          );
        })}
    </>
  );
};

export default KeywordList;
