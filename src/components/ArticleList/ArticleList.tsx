import { IonImg, IonItem, IonLabel, IonList, IonThumbnail } from '@ionic/react';
import { SchemaArticle } from '../../schemas/article';
import { getImageSizeUrl } from '../../helpers/images';
import { CMS_UPLOADS } from '../../constants';

import './ArticleList.css';

type ArticleListProps = {
  data?: Array<SchemaArticle>;
};

const ArticleList: React.FC<ArticleListProps> = ({ data }) => {
  return (
    <IonList>
      {data &&
        data.map(({ slug, title, category, content, featured_image }) => {
          const image_url = featured_image?.url
            ? getImageSizeUrl(`${CMS_UPLOADS}${featured_image.url}`, {
                w: 48,
                h: 48,
              })
            : null;
          return (
            <IonItem key={`article-${slug}`} detail routerDirection="forward" routerLink={`/articulo/${slug}`}>
              <IonLabel>
                <h2>{title}</h2>
              </IonLabel>

              {image_url && (
                <IonThumbnail className="ion-margin-end">
                  <IonImg src={image_url} />
                </IonThumbnail>
              )}
            </IonItem>
          );
        })}
    </IonList>
  );
};

export default ArticleList;
