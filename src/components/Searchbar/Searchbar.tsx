import { IonButton, IonLabel, IonSearchbar, IonToolbar } from '@ionic/react';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ROUTES } from '../../constants';

type SearchbarProps = {
  term?: string;
};

const isTermValid = (term?: string | null) => {
  if (!term) return false;
  return term ? term !== '' && term.length > 2 : false;
};

const Searchbar: React.FC<SearchbarProps> = ({ term }) => {
  const history = useHistory();
  const [searchTerm, setSearchTerm] = useState(term ? term : '');
  const [searchEnabled, setSearchEnabled] = useState(isTermValid(term));

  useEffect(() => {
    setSearchEnabled(isTermValid(searchTerm));
  }, [searchTerm]);

  return (
    <IonToolbar className="ion-padding-top">
      <IonSearchbar
        slot="start"
        placeholder="Ej. huertos"
        value={searchTerm}
        showClearButton="focus"
        enterkeyhint="search"
        onKeyUp={({ key }) => {
          if (key === 'Enter') {
            history.push(`${ROUTES.search}${encodeURI(searchTerm)}`);
          }
        }}
        onIonChange={({ detail }) => {
          setSearchTerm(detail.value!);
        }}
      />
      <IonButton
        slot="end"
        color="secondary"
        disabled={!searchEnabled}
        onClick={() => {
          history.push(`${ROUTES.search}${encodeURI(searchTerm)}`);
        }}
      >
        <IonLabel color="light">Buscar</IonLabel>
      </IonButton>
    </IonToolbar>
  );
};

export default Searchbar;
