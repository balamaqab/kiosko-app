import { IonCard, IonCardContent, IonCol, IonRow, IonSpinner, IonText } from '@ionic/react';
import './LoadingWidget.css';

type LoadingWidgetProps = {
  children?: React.ReactNode;
  className?: string;
  minHeight?: number;
};

const LoadingWidget: React.FC<LoadingWidgetProps> = ({ children, className, minHeight }) => {
  return (
    <>
      <IonCard className={'dimLoadingWidget ' + className}>
        <IonCardContent className="dimContent ion-no-padding">
          <IonRow className="ion-margin-horizontal" style={{ minHeight: minHeight || 96 }}>
            <IonCol size="auto" className=" ion-align-self-center"></IonCol>
            <IonCol className=" ion-text-center ion-align-self-center">
              <IonSpinner className="dimIcon ion-margin-end " name="dots" color="warning" />
              <IonText className="dimLabel ion-margin-start ">{children || 'Loading...'}</IonText>
            </IonCol>
          </IonRow>
        </IonCardContent>
      </IonCard>
    </>
  );
};

export default LoadingWidget;
