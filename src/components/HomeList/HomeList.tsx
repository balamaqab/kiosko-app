import { IonAvatar, IonImg, IonItem, IonLabel, IonList } from '@ionic/react';
import { SchemaHomepage } from '../../schemas/homepage-item';
import { CMS_URL, ROUTES } from '../../constants';

type HomeListProps = {
  data?: SchemaHomepage;
};

const HomeList: React.FC<HomeListProps> = ({ data }) => {
  const items = data?.item || [];
  return (
    <IonList>
      {items &&
        items.map(({ id, description, icon, category, keyword, article }) => {
          const iconUrl = icon ? `${CMS_URL}${icon.url}` : null;
          let title = '',
            url: string | null = null;

          if (article) {
            title = article.title;
            url = ROUTES.article + article.slug;
          } else if (category) {
            title = category.name || '';
            let titleForUrl = category.name ? `/${encodeURI(category.name)}` : '';
            url = ROUTES.category + category.slug + titleForUrl;
          } else if (keyword) {
            title = '#' + keyword.keyword || '';
            let titleForUrl = keyword.keyword ? `/${encodeURI(keyword.keyword)}` : '';
            url = ROUTES.keyword + keyword.slug + titleForUrl;
          }

          return (
            url && (
              <IonItem key={`item-${id}`} detail routerDirection="forward" routerLink={url}>
                {iconUrl && (
                  <IonAvatar className="ion-margin">
                    <IonImg src={iconUrl} />
                  </IonAvatar>
                )}

                <IonLabel>
                  <h2>{title}</h2>
                  {description && <p>{description}</p>}
                </IonLabel>
              </IonItem>
            )
          );
        })}
    </IonList>
  );
};

export default HomeList;
