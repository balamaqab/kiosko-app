import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCol,
  IonImg,
  IonRouterLink,
  IonRow,
  IonText,
} from '@ionic/react';
import { CMS_URL } from '../../constants';
import { getImageSizeUrl } from '../../helpers/images';
import { SchemaArticle } from '../../schemas/article';
import Markdown from 'marked-react';
import { renderer } from '../../helpers/marked-render';

import './ArticleContent.css';
import KeywordList from '../KeywordList';
import Searchbar from '../Searchbar';

type ArticleContentProps = {
  data?: Array<SchemaArticle>;
};

const ArticleContent: React.FC<ArticleContentProps> = ({ data }) => {
  if (!data || (data && data.length < 1)) {
    return (
      <IonCard>
        <IonCardHeader>
          <IonCardTitle>Artículo no encontrado</IonCardTitle>
        </IonCardHeader>
        <IonCardContent>
          Lo sentimos, el artículo que buscas no existe. Intenta usar el buscador para encontrar información:
          <Searchbar />
        </IonCardContent>
      </IonCard>
    );
  }

  const { title, content, keywords, category, featured_image, updatedAt } = data[0];
  return (
    <>
      <IonCard>
        {featured_image && (
          <IonImg
            src={featured_image.url ? getImageSizeUrl(`${CMS_URL}${featured_image.url}`, { resize: '640x240' }) : ''}
            alt={featured_image.alternativeText}
          />
        )}
        <IonCardHeader>
          {category?.name && (
            <IonCardSubtitle>
              <IonRouterLink routerLink={`/tema/${category?.slug}`} routerDirection="forward">
                {category?.name}
              </IonRouterLink>
            </IonCardSubtitle>
          )}
          {title && <IonCardTitle>{title}</IonCardTitle>}
        </IonCardHeader>
        <IonCardContent>
          <Markdown renderer={renderer}>{content}</Markdown>

          {updatedAt && (
            <p className="subtle">
              <strong>Última actualización:</strong> {new Date(updatedAt).toLocaleDateString('es-GT')}
            </p>
          )}
        </IonCardContent>
      </IonCard>

      {keywords && keywords.length > 0 && (
        <>
          <IonRow className="ion-margin">
            <IonCol>
              <IonText className="ion-margin-top" color="dark">
                <h5>Palabras clave:</h5>
              </IonText>
              <KeywordList data={keywords} />
            </IonCol>
          </IonRow>
        </>
      )}
    </>
  );
};

export default ArticleContent;
