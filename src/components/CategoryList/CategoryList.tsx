import { IonAvatar, IonImg, IonItem, IonLabel, IonList } from '@ionic/react';
import { SchemaCategory } from '../../schemas/category';
import { CMS_URL } from '../../constants';

type CategoryListProps = {
  data?: Array<SchemaCategory>;
};

const CategoryList: React.FC<CategoryListProps> = ({ data }) => {
  return (
    <IonList>
      {data &&
        data.map(({ slug, name, description, icon }) => {
          const title = name ? `/${encodeURI(name)}` : '';
          const iconUrl = icon ? `${CMS_URL}${icon.url}` : null;
          return (
            <IonItem key={`category-${slug}`} detail routerDirection="forward" routerLink={`/tema/${slug}${title}`}>
              {iconUrl && (
                <IonAvatar className="ion-margin">
                  <IonImg src={iconUrl} />
                </IonAvatar>
              )}

              <IonLabel>
                <h2>{name}</h2>
                <p>{description}</p>
              </IonLabel>
            </IonItem>
          );
        })}
    </IonList>
  );
};

export default CategoryList;
