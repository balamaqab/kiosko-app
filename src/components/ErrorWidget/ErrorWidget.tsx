import { IonCard, IonCardContent, IonCol, IonRow, IonText } from '@ionic/react';
import './ErrorWidget.css';

type ErrorWidgetProps = {
  children?: React.ReactNode;
  className?: string;
  minHeight?: number;
};

const ErrorWidget: React.FC<ErrorWidgetProps> = ({ children, className, minHeight }) => {
  return (
    <>
      <IonCard className={'dimErrorWidget ' + className}>
        <IonCardContent className="dimContent ion-no-padding">
          <IonRow className="ion-margin-horizontal" style={{ minHeight: minHeight || 96 }}>
            <IonCol size="auto" className=" ion-align-self-center"></IonCol>
            <IonCol className=" ion-text-center ion-align-self-center">
              <IonText className="dimLabel ion-margin-start ">{children || 'Unexpected error'}</IonText>
            </IonCol>
          </IonRow>
        </IonCardContent>
      </IonCard>
    </>
  );
};

export default ErrorWidget;
