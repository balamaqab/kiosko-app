import { IonBackButton, IonButtons, IonImg, IonRouterLink, IonTitle, IonToolbar } from '@ionic/react';
import { chevronBack } from 'ionicons/icons';
import { RouteComponentProps } from 'react-router';

import './Header.css';

const KskHeader: React.FC<RouteComponentProps> = ({ match }) => {
  return (
    <IonToolbar className="ksk-toolbar">
      {match.path !== '/inicio' && (
        <IonButtons slot="start">
          <IonBackButton defaultHref="/inicio" color="primary" icon={chevronBack} text="Atrás" />
        </IonButtons>
      )}
      <IonTitle className="ksk-toolbar-title" slot={match.path !== '/inicio' ? 'end' : ''}>
        <IonRouterLink routerDirection="root" routerLink="/">
          <IonImg className="ksk-toolbar-logo" src="/assets/kioskito.png" alt="Kioskito de Yo'o Guatemala" />
        </IonRouterLink>
      </IonTitle>
    </IonToolbar>
  );
};

export default KskHeader;
