import useFetchContent from '../../hooks/useFetchContent';
import ErrorWidget from '../ErrorWidget/ErrorWidget';
import LoadingWidget from '../LoadingWidget/LoadingWidget';

type StrapiQueryProps = {
  endpoint: string;
  limit?: number;
  customQuery?: any;
  children: any;
};

const StrapiQuery: React.FC<StrapiQueryProps> = ({ endpoint, limit, children, customQuery }) => {
  const { content, isLoading, isError } = useFetchContent({
    endpoint: `/${endpoint}`,
    query: {
      pagination: {
        limit: limit ? limit : 25,
      },
      ...customQuery,
    },
  });

  if (isError) return <ErrorWidget />;
  if (isLoading) return <LoadingWidget />;

  return typeof children === 'function' ? children(content) : children;
};

export default StrapiQuery;
