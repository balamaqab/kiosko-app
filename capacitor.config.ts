import { CapacitorConfig } from "@capacitor/cli";

const config: CapacitorConfig = {
  appId: "org.yo-o.kiosko",
  appName: "kiosko",
  webDir: "build",
  bundledWebRuntime: false,
};

export default config;
